import pyqtgraph as pg

import pyqtgraph.exporters
import numpy as np
import torchfile as t7
import sys

np.set_printoptions(suppress=True)
if len(sys.argv) == 1:
    print ('usage:  python visualize_matrices matrix_file.dat')

pg.setConfigOptions(antialias=True)
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

app = pg.QtGui.QApplication([])
win = pg.TableWidget()
data = t7.load(sys.argv[1]).astype(np.float32)

#data = data / np.sum(data)

def update():
    global data
    global win
    win.setData(data)
    win.setFormat('%d')

    win.show()

if __name__ == '__main__':

    timer = pg.QtCore.QTimer()
    timer.setInterval(100)
    timer.setSingleShot(True)
    timer.timeout.connect(update)

    update()
    timer.start()

    if sys.flags.interactive != 1 or not hasattr(pg.QtCore, 'PYQT_VERSION'):
        pg.QtGui.QApplication.exec_()
