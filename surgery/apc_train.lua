require 'torch'		-- duh
require 'nn'		-- neural networks
require 'nngraph'
require 'optim'		-- optimization
require 'image'		-- image processing, load, save, scale, ...
require 'cunn'		-- cuda. required for GPU train/test
require 'lfs'		-- file system: create dir, etc.
require 'util.misc'	-- Anton's helper functions
require 'apc'		-- For test code
require 'io'        -- For log file writing
-- optnet = require 'optnet' -- optimize net

matio = require 'matio'  -- Matlab IO interface

--[[
Train image segmentation based on 
[Husain et al., Combining Semantic and Geometric Features 
for Object Class Segmentation of Indoor Scenes, ICRA 2016]
--]]

-- nngraph.setDebug(true)  -- uncomment for debug mode
torch.setdefaulttensortype('torch.FloatTensor')

function create_network(nb_output, upsample)
    local filter_size = 11 -- fixed filter size
    local pad = (filter_size-1) / 2
    local net = nn.Sequential();  -- make a multi-layer structure
    net:add(nn.SpatialConvolutionMM(256+96,128,filter_size,filter_size,1,1,pad)) -- (256+96) -> 96
    net:add(nn.PReLU())
    net:add(nn.Dropout(params.dropout))
    net:add(nn.SpatialConvolutionMM(128,64,filter_size,filter_size,1,1,pad)) -- 96 -> 32
    net:add(nn.PReLU())
    net:add(nn.SpatialConvolutionMM(64,nb_output,filter_size,filter_size,1,1,pad)) -- 32x120x160 -> Clx120x160
    net:add(nn.SpatialUpSamplingNearest(upsample))
    net:add(nn.Transpose({1,2},{2,3})) -- HxWxCl
    net:add(nn.Reshape(params.imHeight*params.imWidth, nb_output)) -- (H*W)xCl
    net:add(nn.LogSoftMax())
    if params.cuda_device > 0 then net:cuda() end
    return net
end --function create_network


-- Train a Neural Network
function train_network()
    local parameters,gradParameters = network:getParameters()
    network:training()	-- set flag for dropout

    local bs = 1 -- batchsize

    -- hyper-parameters
    local lR = params.learning_rate / torch.sqrt(bs)
    local optimConfig = {learningRate = params.learning_rate,
    momentum = params.momentum,
    learningRateDecay = params.lr_decay}

    -- count number of train instances
    local nfiles = train_data.imArray:size(1)

    print( "Training the network with "..nfiles.." files and "..parameters:nElement().. " parameters" )
    local weights = torch.Tensor(#classes):fill(1)

    -- set loss weights anti-proportional to frequency
    -- TODO: Try median frequency ratio!
    for c=1,#classes-1 do  weights[c] = 1-train_ratios[c] end
    if params.setting == 'shelf' then weights[#classes] = 0 end -- unknown class
    -- (only ignore for shelf because objects outside are not annotated. For box, there are no objects outside box)


    local critNLL =  nn.ClassNLLCriterion(weights)
    local criterion = critNLL

    if params.cuda_device >0 then criterion = criterion:cuda() end

    -- assign input and target
    local inputs = train_data_clone.imArray
    local features = hha_train_clone.hha
    local targets = train_data.labelArray

    print ( getModelSignature())

    local train_loss_logger = optim.Logger('logs/train_loss_' .. getModelSignature())
    train_loss_logger:setNames{'Loss'}
    train_loss_logger:style{'+-' }

    local train_gradNorm_logger = optim.Logger('logs/train_gradNorm_' .. getModelSignature())
    train_gradNorm_logger:setNames{'GradNorm'}
    train_gradNorm_logger:style{'+-' }

    validation_f1_logger = optim.Logger('logs/validation_f1_'.. getModelSignature())
    validation_f1_logger:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    validation_f1_logger:style{'+-', '+-', '+-' }

    train_f1_logger = optim.Logger('logs/train_f1_'.. getModelSignature())
    train_f1_logger:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    train_f1_logger:style{'+-', '+-', '+-' }

    validation_f1_logger_all = optim.Logger('logs/validation_f1_all'.. getModelSignature())
    validation_f1_logger_all:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    validation_f1_logger_all:style{'+-', '+-', '+-' }

    train_f1_logger_all = optim.Logger('logs/train_f1_all'.. getModelSignature())
    train_f1_logger_all:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    train_f1_logger_all:style{'+-', '+-', '+-'}

    for ep=1,params.epochs do
        EPOCH = ep
        local shuffle = torch.randperm(nfiles)
        for ii = 1,nfiles,bs do
            xlua.progress(ii, nfiles)
            local t = shuffle[ii]
            local color_image = inputs[{t,{1,3},{},{}}]
            local depth_image2 = features[{t,{},{},{}}]

            local target_image = targets[{t,{},{}}]

            cutorch.setDevice(params.cuda_device_train)
            local input = network0:forward(color_image)	-- process RGB
            local inputd2 = networkd:forward(depth_image2):squeeze()   -- HHA
            local input_concat = torch.cat(input,inputd2,1):squeeze()  -- concat RGB,  HHA
            cutorch.setDevice(params.cuda_device)
            collectgarbage('collect')

            input_concat = input_concat:clone():cuda()

            local target = target_image:reshape(params.imWidth * params.imHeight) -- reshape target as vector

            -- create closure to evaluate f(X) and df/dX
            local loss = 0
            local feval = function(x)
                -- get new parameters
                if x ~= parameters then parameters:copy(x) end
                collectgarbage()

                -- reset gradients
                gradParameters:zero()
                -- f is the average of all criterions
                -- evaluate function for complete mini batch

                local output = network:forward(input_concat)	-- run forward pass
                local err = criterion:forward(output, target)	-- compute loss

                loss = loss + err

                -- estimate df/dW
                local df_do = criterion:backward(output, target)

                network:backward(input_concat, df_do)		-- update parameters

                --display result
                --local win_input = showImage(color_image, win_input)
                --local win_target = showImage(colorizeLabels(target_image:float()), win_target)

                local _,predicted_labels = torch.max(output,2)
                predicted_labels = torch.reshape(predicted_labels:squeeze():float(),params.imHeight,params.imWidth)
                predicted_labels = colorizeLabels(predicted_labels:float()):float()
                --local win_pl = showImage(predicted_labels, win_pl)

                local woohooString = ''
                pm(string.format('Training loss: %.4f, %s', loss, woohooString), 2)

                --clip the gradients to avoid exploding gradients problem
                gradParameters:clamp(-params.grad_clip,params.grad_clip)

                if loss < 0.3 then train_loss_logger:add{loss} else train_loss_logger:add{0.3} end
                train_loss_logger:plot()
                train_gradNorm_logger:add{torch.norm(gradParameters)}
                train_gradNorm_logger:plot()

                return err,gradParameters
            end -- feval lambda function

            _,current_loss = optim.sgd(feval, parameters, optimConfig)
            collectgarbage('collect')
        end -- ii


        print(string.format('Loss: %.4f  Epoch: %d   grad-norm: %.4f',
        current_loss[1], ep, torch.norm(parameters)/torch.norm(gradParameters)))

        if (ep % 5) == 0 or ep == params.epochs then
            local filename = getModelFilename()
            if ep < params.epochs then filename = getModelFilename(ep) end

            pm('Saving checkpoint to '..filename .. '...')
            network:clearState()
            torch.save(filename, network)

            pm('Training')
            test_predictor_all(classes, 'train')
            pm('Validation...')
            test_predictor_all(classes, 'validation')
        end -- if epoch output
        collectgarbage('collect')
    end --epochs
    print('Training completed')
end -- function train_network

function printCombWeights(module)
    local wt = module.weight
    local bs = module.bias
    print(string.format('%8s%8s%8s%8s %s','Wt Seg','Wt DC','Wt ARst','Bias','Class'))
    for c=1,params.num_classes do
        local wtSeg = wt[c][c]
        local wtDC = wt[c][c+params.num_classes]
        local wtRest = wt[c]:abs():sum() - torch.abs(wtSeg) - torch.abs(wtDC)
        local bias = bs[c]
        print(string.format('%8.4f%8.4f%8.4f%8.4f %s',wtSeg,wtDC,wtRest,bias,classes_names[c]))
    end
end -- function printCombWeights


-- mode is switch between training data and validation data
-- mode possible values  =  train / validation
function test_predictor_all(classes, mode)
    print('Predict all images mode ======>' .. mode)
    if mode ~= 'validation' and mode ~= 'train' then
        print('Fatal!!! mode is not validation or train')
        abort()
    end

    testConfusion  = optim.ConfusionMatrix(classes)
    testConfusion2 = torch.zeros(#classes,#classes)

    local bs=1 -- batch size

    --switch the network to evaluate mode(turns off DropOut)
    network:evaluate()

    local nfiles, inputs, features, targets
    if mode == 'train' then
        nfiles = train_data.imArray:size(1)
        inputs = train_data_clone.imArray
        features = hha_train_clone.hha
        targets = train_data.labelArray
	print ( targets:size())
    else -- mode validate
        nfiles = test_data.imArray:size(1)
        inputs = test_data_clone.imArray
        features = hha_test_clone.hha
        targets = test_data.labelArray
    end

    for t = 1,nfiles,bs do
        xlua.progress(t, nfiles)	-- progress bar

        local inpIm = inputs[{t,{1,3},{},{}}]
        local featIm = features[{t,{},{},{}}]
        local target = targets[{t,{},{}}]

        -- test one image
        cutorch.setDevice(params.cuda_device_train)
        local input = network0:forward(inpIm)	-- process RGB
        local inputd2 = networkd:forward(featIm):squeeze()   -- HHA
        local input_concat = torch.cat(input,inputd2,1):squeeze()  -- concat RGB,  HHA
        cutorch.setDevice(params.cuda_device)
        input_concat = input_concat:clone():cuda()

        local output = network:forward(input_concat)	-- run forward pass

        local _,predicted_labels = torch.max(output,2)
        predicted_labels = torch.reshape(predicted_labels:squeeze():float(),params.imHeight,params.imWidth)

        update_confusion2(testConfusion2, predicted_labels, target, classes)

--        local _,predicted_labels = torch.max(output,2)
--        predicted_labels = torch.reshape(predicted_labels:squeeze():float(),params.imHeight,params.imWidth)
--        local unwarpedLabels = unwarpImage(predicted_labels, cropInfo.transformInfo, cropInfo)
--        unwarpedLabels = colorizeLabels(unwarpedLabels)

        image.save('predictions/'..EPOCH..'_'..t..'prediction.png', colorizeLabels(predicted_labels))
        image.save('predictions/'..EPOCH..'_'..t..'target.png',colorizeLabels(target))
    end

    torch.save('logs/confusion_matrix_'.. EPOCH .. '.dat',testConfusion2)

    testConfusion.mat = testConfusion2
    if #classes < 15 then print(testConfusion) end
    local precesion, recall, f1Score =  compute_F1score(testConfusion2)

    torch.save('logs/'.. mode ..'_precesion_' .. EPOCH .. '.dat', precesion)
    torch.save('logs/'.. mode ..'_recall_' .. EPOCH .. '.dat', recall)
    torch.save('logs/'.. mode ..'_f1_' .. EPOCH .. '.dat', f1Score)
    image.save('predictions/'.. mode ..'_confusion_matrix_'.. EPOCH ..'.png', testConfusion:render()) -- render matrix

    if mode == 'train' then
        train_f1_logger:add{ (precesion[CLASSBOOLINDICES]:sum()/#classes) * 100,
            (recall[CLASSBOOLINDICES]:sum()/#classes) * 100, (f1Score[CLASSBOOLINDICES]:sum()/#classes) * 100 }
        train_f1_logger:plot()
        train_f1_logger_all:add{ (precesion:sum()/#classes) * 100,
            (recall:sum()/#classes) * 100, (f1Score:sum()/#classes) * 100 }
        train_f1_logger_all:plot()

    else -- mode validation
        validation_f1_logger:add{ (precesion[CLASSBOOLINDICES]:sum()/#classes) * 100,
            (recall[CLASSBOOLINDICES]:sum()/#classes) * 100, (f1Score[CLASSBOOLINDICES]:sum()/#classes) * 100 }
        validation_f1_logger:plot()
        validation_f1_logger_all:add{ (precesion:sum()/#classes) * 100,
            (recall:sum()/#classes) * 100, (f1Score:sum()/#classes) * 100 }
        validation_f1_logger_all:plot()
    end
end

-- Input: confusion matrix
-- output: array of Precesion, Recall, F1 score for each object
function compute_F1score(confusionMatrix)
    local precesion, recall, f1Score
    precesion = torch.Tensor(#classes):zero()
    recall    = torch.Tensor(#classes):zero()
    f1Score   = torch.Tensor(#classes):zero()
    for i = 1, #classes do
        if confusionMatrix[i]:sum() ~= 0 then
            precesion[i]  =  confusionMatrix[{i,i}] / confusionMatrix[i]:sum() else precesion[i] = 0 end
        if confusionMatrix[{{},{i}}]:sum() ~= 0 then
            recall[i] = confusionMatrix[{i,i}] / confusionMatrix[{{},{i}}]:sum() else recall[i] = 0 end
        if precesion[i] + recall[i] ~= 0 then
            f1Score[i] = (2 * precesion[i] * recall[i]) / (precesion[i] + recall[i]) else f1Score[i] = 0 end
    end
    return precesion, recall, f1Score
end


function update_confusion2(testConfusion2, predicted_labels, target, classes)
    -- loop over all locations
    local confusion = torch.zeros(#classes,#classes)
    if params.cuda_device > 0 then
        predicted_labels = predicted_labels:float()
        target = target:float()
    end
    for i = 1,#classes-1 do
        for j = 1,#classes-1 do
            -- no. of predicted labels for class j
            local np = torch.sum(torch.eq(target,i):cmul(torch.eq(predicted_labels,j)))
            confusion[{i,j}] = np
        end
    end
    testConfusion2:add(confusion)
end -- function update_confusion2



function main()
    if runFromCmdLine() then setParams() end
    createAuxDirs() -- create required directories quietly
    pm('Running segmentation of '..params.num_classes..' classes on '..params.setting, 2)

    classes, classes_names = getClassInfo()
    getBooleanClassIndex()

    local dtype, use_cudnn = setup_gpus(params.cuda_device, 0)

    if params.mode=='train' then
        train_data, test_data, hha_train, hha_test = dofile('apc_dataset.lua')
    else
        test_data, hha_test, cropInfo = processInput(params.imDir)
        image.save('predictions/rgb.png', cropInfo.rgbIm)
        if params.cuda_device > 0 then
            test_data.imArray = test_data.imArray:cuda()
            test_data.labelArray = test_data.labelArray:cuda()
            hha_test.hha = hha_test.hha:cuda()
        end
    end

    cutorch.setDevice(params.cuda_device_train)
    loadPretrainedModels() -- load OverFeat model parameters
    cutorch.setDevice(params.cuda_device)

    if params.mode == 'train' then  -- train mode. Create network and off we go...
        network = create_network(#classes, params.downsample)
        -- graph.dot(network.fg, 'ForwardNet', 'MM_Network')
        train_network()
    else	-- testing

    local networkFileName = getModelFilename()
    pm('Loading '..networkFileName)
    network = torch.load(networkFileName)	-- load the network
    if params.cuda_device == 0 then
        network = network:float()
        print('converted to cpu')
    end

    collectgarbage()
    printGPUStatus('after garbage collection')

    test_predictor_all(network, test_data, hha_test, classes, params.downsample, params.jitter)
end

-- keep display and terminate
if runFromCmdLine() then sleep(1); os.exit()  end
end

-- if run on command line do main()
main()
