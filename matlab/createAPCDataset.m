%% shelf hard
addpath(genpath('./external'))  % dependencies
[APCDataDir, APCOutDataDir] = getAPCDataDir();
instCnt=0;
allFilesShelf = {};
unknownClassID = 43; %% All object inclusive 44. Exculding ignore kleenex_tissue_box
doShelf = true;
doTote = false;
[y,m,d] = datevec(date);

% enable this to use add DenseCap probability maps 
doDenseCap = false;

% mainName = 'apc_lowres_hha_0622';
mainName = sprintf('apc_lowres_%02d%02d',m,d);

%% shelf hard
if doShelf
    fprintf('--- SHELF  HARD ---\n');
    capturesDir = [APCDataDir,'shelf/20160424_hard/'];
    imagesDir = dir([capturesDir,'bin_*']);
    for imD = 1:length(imagesDir)
        instanceDirs = dir([capturesDir,imagesDir(imD).name,filesep,'image*']);
  
        fprintf('Processing %s...\n',imagesDir(imD).name);
        for inst = 1:length(instanceDirs)
            %%% ignore kleenex_tissue_box and
            %%% platinum_pets_dog_bowl %%%
            
            %%% loop through the files in the ImagesDirName and ignore if
            %%% the files with above names %%%
            
            ImagesDirName  = fullfile('shelf','20160424_hard',imagesDir(imD).name, instanceDirs(inst).name);
            ImagesDirName = strcat(ImagesDirName,'/');
            canAdd = true;
            FullDirName = [APCDataDir,ImagesDirName];
            allFiles = dir([FullDirName, 'mask*']);
            for index = 1:length(allFiles)
                FileName  = allFiles(index).name; 
                if contains(FileName, 'kleenex_tissue_box') % | contains(FileName, 'platinum_pets_dog_bowl')
                    canAdd =  false;
                    break;
                end
            end
            
            if canAdd == true
                instCnt=instCnt+1;        
                allFilesShelf{instCnt} = ImagesDirName; 
                fprintf('Processing %s...\n',instanceDirs(inst).name);
                cropBox;
            else
                fprintf('not added %s \n',ImagesDirName)
            end
            
        end
    end


    %% shelf captures
    fprintf('--- SHELF  ---\n');
    capturesDir = [APCDataDir,'shelf/'];
    imagesDir = dir([capturesDir,'capture_*']);
    for imD = 1:length(imagesDir)
        instanceDirs = dir([capturesDir,imagesDir(imD).name,filesep,'image*']);
        fprintf('Processing %s...\n',imagesDir(imD).name);
        for inst = 1:length(instanceDirs)
            %%% ignore kleenex_tissue_box and
            %%% platinum_pets_dog_bowl %%%
            
            %%% loop through the files in the ImagesDirName and ignore if
            %%% the files with above names %%%
            ImagesDirName  = fullfile('shelf',imagesDir(imD).name, instanceDirs(inst).name);
            ImagesDirName = strcat(ImagesDirName,'/');
            canAdd = true;
            FullDirName = [APCDataDir,ImagesDirName];
            allFiles = dir([FullDirName, 'mask*']);
            for index = 1:length(allFiles)
                FileName  = allFiles(index).name; 
                if contains(FileName, 'kleenex_tissue_box') %| contains(FileName, 'platinum_pets_dog_bowl')
                    canAdd =  false;
                    break;
                end
            end
            if canAdd == true
                instCnt=instCnt+1;        
                allFilesShelf{instCnt} = ImagesDirName; 
                fprintf('Processing %s...\n',instanceDirs(inst).name);
                cropBox;
            else
                fprintf('not added %s \n',ImagesDirName)
            end
        end
    end
    allFilesShelf = allFilesShelf'; % make Nx1 cell array
end

%% tote captures
if doTote
    instCnt=0;
    unknownClassID = 41;
    allFilesTote = {};
    fprintf('--- TOTE  ---\n');
    capturesDir = [APCDataDir,'tote/'];
    imagesDir = dir([capturesDir,'capture_*']);
    for imD = 1:length(imagesDir)
        instanceDirs = dir([capturesDir,imagesDir(imD).name,filesep,'image*']);
        fprintf('Processing %s...\n',imagesDir(imD).name);
        for inst = 1:length(instanceDirs)
            %%% ignore kleenex_tissue_box and
            %%% platinum_pets_dog_bowl %%%
            
            %%% loop through the files in the ImagesDirName and ignore if
            %%% the files with above names %%%
            ImagesDirName  =fullfile('tote',imagesDir(imD).name, instanceDirs(inst).name);
            ImagesDirName = strcat(ImagesDirName,'/');
            canAdd = true;
            FullDirName = [APCDataDir,ImagesDirName];
            allFiles = dir([FullDirName, 'mask*']);
            for index = 1:length(allFiles)
                FileName  = allFiles(index).name; 
                if contains(FileName, 'kleenex_tissue_box') | contains(FileName, 'platinum_pets_dog_bowl')
                    canAdd =  false;
                    break;
                end
            end
            
            if canAdd == true
                instCnt=instCnt+1;        
                allFilesShelf{instCnt} = ImagesDirName; 
                fprintf('Processing %s...\n',instanceDirs(inst).name);
                cropBox;
            else
                fprintf('not added %s \n',ImagesDirName)
            end
        end
    end
    fprintf('--- TOTE   New  ---\n');
    capturesDir = [APCDataDir,'tote/'];
    imagesDir = dir([capturesDir,'run_*']);
    for imD = 1:length(imagesDir)
        instanceDirs = dir([capturesDir,imagesDir(imD).name,filesep,'image*']);
        fprintf('Processing %s...\n',imagesDir(imD).name);
        for inst = 1:length(instanceDirs)
            %%% ignore kleenex_tissue_box and
            %%% platinum_pets_dog_bowl %%%
            
            %%% loop through the files in the ImagesDirName and ignore if
            %%% the files with above names %%%
            ImagesDirName  =fullfile('tote',imagesDir(imD).name, instanceDirs(inst).name);
            ImagesDirName = strcat(ImagesDirName,'/');
            canAdd = true;
            FullDirName = [APCDataDir,ImagesDirName];
            allFiles = dir([FullDirName, 'mask*']);
            for index = 1:length(allFiles)
                FileName  = allFiles(index).name; 
                if contains(FileName, 'kleenex_tissue_box') | contains(FileName, 'platinum_pets_dog_bowl')
                    canAdd =  false;
                    break;
                end
            end
            
            if canAdd == true
                instCnt=instCnt+1;        
                allFilesShelf{instCnt} = ImagesDirName; 
                fprintf('Processing %s...\n',instanceDirs(inst).name);
                cropBox;
            else
                fprintf('not added %s \n',ImagesDirName)
            end
        end
    end
    
    allFilesTote = allFilesTote'; % make Nx1 cell array
end

%% put everything into mat files for training
doSetting = [];
if doShelf, doSetting = [doSetting, 1]; end
if doTote, doSetting = [doSetting, 2]; end
rng(321)

% datasetDir is same as getAPCDataDir->APCOutDataDir
datasetDir = '/home/data/stud/periyasa/matlab_ws/';
TrainingRatio = .8;
for ss=doSetting % shelf / tote    
    
    setting = 'shelf';
    filesArray = allFilesShelf;
    if ss==2
        setting='tote'; 
        filesArray = allFilesTote;
    end
    nClasses = getNumClasses(setting);
    fprintf('Setting: %s\n',setting);
    
    % save all files
    fileID = fopen(sprintf('%s/%s_%s_all_files.txt', datasetDir,mainName,setting),'w');
    [nrows,ncols] = size(filesArray);
    for row = 1:nrows, fprintf(fileID,'%s\n',filesArray{row}); end

    nSamples = length(dir(sprintf('%s/%s/*rgb*',datasetDir,setting)));
    allIdx = randperm(nSamples);
    nTraining = round(nSamples*TrainingRatio);
    trngIdx = allIdx(1:nTraining);
    testIdx = allIdx(nTraining+1:end);
    nTest = length(testIdx); nTraining=length(trngIdx); 

    for tt=1:3 % train / test / all
        trainTestMode = 'train'; 
        idx = trngIdx; 
        
        if tt==2 
            idx=testIdx; 
            trainTestMode='test';
        elseif tt==3
            idx = allIdx;
            trainTestMode='all';
        end
        fprintf('Mode: %s\n',trainTestMode);
     
        
        % go training
        imArray=zeros(targetHeight,targetWidth,3,length(idx));
        labelArray=zeros(targetHeight,targetWidth,length(idx),'uint8');
        depths=zeros(targetHeight,targetWidth,length(idx));
        hha=zeros(targetHeight,targetWidth,3,length(idx));
        if doDenseCap == true
            dcprobs=zeros(targetHeight,targetWidth,nClasses,length(idx));
        end
        for t = 1:length(idx)
            imArray(:,:,:,t)=double(imread(sprintf('%s/%s/%06d_rgb.png',datasetDir,setting,idx(t))))/255;
            labelArray(:,:,t)=imread(sprintf('%s/%s/%06d_labels.png',datasetDir,setting,idx(t)));
%             depths(:,:,t)=double(imread(sprintf('%s/%s/%06d_depth.png',datasetDir,setting,idx(t))))/255;
            load(sprintf('%s/%s/%06d_features.mat',datasetDir,setting,idx(t)));
            
            imArray=single(imArray); 
  %         dcprobs=single(dcprobs);

            labelArray = uint8(labelArray); 
            featDepth = single(featDepth); 
            featHHA = single(featHHA); 
        
            depths(:,:,t)=featDepth;
            hha(:,:,:,t)=featHHA;
            
            if doDenseCap == true 
                dcprobs(:,:,:,t) = featDensecap;
                dcFilename = sprintf('%s/%s/%06d_densecap.mat',datasetDir,setting,idx(t));
                dcprobs(:,:,:,t)=featDensecap;
            end

        end
        labelArray(labelArray==0)=nClasses;
        labelArray(labelArray>nClasses)=nClasses;
        
        imArray=permute(imArray,[2,1,3,4]); 
       if doDenseCap == true 
         dcprobs=permute(dcprobs,[2,1,3,4]);
       end
        labelArray=permute(labelArray,[2,1,3]);
        depths=permute(depths,[2,1,3]);
        hha=permute(hha,[2,1,3,4]);

       if doDenseCap == true 
            dcprobs1=dcprobs(:,:,1:10,:);
            dcprobs2=dcprobs(:,:,11:20,:);
            dcprobs3=dcprobs(:,:,21:30,:);
            dcprobs4=dcprobs(:,:,31:41,:);
            clear dcprobs
       end
        
%         save(sprintf('%s/%s_%s_%s.mat',datasetDir,mainName,setting,trainTestMode),'imArray','labelArray','depths','hha','d2s','rmd','dcprobs','idx','filesArray*','-v7.3');
     if doDenseCap == true 
        save(sprintf('%s/%s_%s_%s.mat',datasetDir,mainName,setting,trainTestMode),'imArray','labelArray','depths','hha','dcprobs*','idx','filesArray*','-v7.3');
     else
         save(sprintf('%s/%s_%s_%s.mat',datasetDir,mainName,setting,trainTestMode),'imArray','labelArray','depths','hha','idx','filesArray*','-v7.3');
     end
         
        filesArraySubset = filesArray(idx,:);
        % save train / test split        
        fileID = fopen(sprintf('%s/%s_%s_%s_files.txt',datasetDir,mainName,setting,trainTestMode),'w');
        [nrows,ncols] = size(filesArraySubset);
        for row = 1:nrows, fprintf(fileID,'%s\n',filesArraySubset{row}); end        
        
        % LIGHT SUBSET
        n = min(length(idx),5);
        imArray=imArray(:,:,:,1:n);
        labelArray=labelArray(:,:,1:n);        
        depths=depths(:,:,1:n); 
        hha=hha(:,:,:,1:n);
        if doDenseCap == true 
            dcprobs1=dcprobs1(:,:,:,1:n); 
            dcprobs2=dcprobs2(:,:,:,1:n); 
            dcprobs3=dcprobs3(:,:,:,1:n); 
            dcprobs4=dcprobs4(:,:,:,1:n); 
        end
        
        idx=idx(1:n); 
        filesArraySubset = filesArraySubset(1:n,:);
        if doDenseCap == true 
            save(sprintf('%s/%s_%s_%s_light.mat',datasetDir,mainName,setting,trainTestMode),'imArray','labelArray','depths','hha','dcprobs*','idx','filesArray*','-v7.3');
        else
            save(sprintf('%s/%s_%s_%s_light.mat',datasetDir,mainName,setting,trainTestMode),'imArray','labelArray','depths','hha','idx','filesArray*','-v7.3');
        end
        
        
        % save train / test split        
        fileID = fopen(sprintf('%s/%s_%s_%s_files_light.txt',datasetDir,mainName,setting,trainTestMode),'w');
        [nrows,ncols] = size(filesArraySubset);
        for row = 1:nrows, fprintf(fileID,'%s\n',filesArraySubset{row}); end
    
    end
end
