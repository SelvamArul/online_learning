
% For training online Learning, I am excluding all the files containing
% kleenex_tissue_box and platinum_pets_dog_bowl
function apcObj = getAPCObjList
apcObj = {...
	'barkely_hide_bones', ...                   % 1
	'box', ...                                  % 2
	'cherokee_easy_tee_shirt', ...              % 3
	'clorox_utility_brush', ...                 % 4
	'cloud_b_plush_bear', ...                   % 5
	'command_hooks', ...                        % 6
	'cool_shot_glue_sticks', ...                % 7
	'crayola_24_ct', ...                        % 8
	'creativity_chenille_stems', ...            % 9
	'dasani_water_bottle', ...                  % 10
	'dove_beauty_bar', ...                      % 11
	'dr_browns_bottle_brush', ...               % 12
	'easter_turtle_sippy_cup', ...              % 13
	'elmers_washable_no_run_school_glue', ...   % 14
	'expo_dry_erase_board_eraser', ...          % 15
	'fiskars_scissors_red', ...                 % 16
	'fitness_gear_3lb_dumbbell', ...            % 17
	'folgers_classic_roast_coffee', ...         % 18
	'hanes_tube_socks', ...                     % 19
	'i_am_a_bunny_book', ...                    % 20
	'jane_eyre_dvd', ...                        % 21 
    'kleenex_paper_towels', ...                 % 22  'kleenex_tissue_box', ...                   % 23
	'kyjen_squeakin_eggs_plush_puppies', ...    % 23
	'laugh_out_loud_joke_book', ...             % 24
	'oral_b_toothbrush_green', ...              % 25
	'oral_b_toothbrush_red', ...                % 26
	'peva_shower_curtain_liner', ...            % 27 
    'platinum_pets_dog_bowl', ...               % 29
	'rawlings_baseball', ...                    % 28
	'rolodex_jumbo_pencil_cup', ...             % 29
	'safety_first_outlet_plugs', ...            % 30
	'scotch_bubble_mailer', ...                 % 31
	'scotch_duct_tape', ...                     % 32
	'soft_white_lightbulb', ...                 % 33
	'staples_index_cards', ...                  % 34
	'ticonderoga_12_pencils', ...               % 35
	'up_glucose_bottle', ...                    % 36
	'womens_knit_gloves', ...                   % 37
	'woods_extension_cord', ...                 % 38
	'front_bar', ...                            % 39
	'side_bar', ...                             % 40
	'ground_metal' ...                          % 41
    }';                   
end