function [APCDataDir, APCOutDataDir] = getAPCDataDir()

% sensor recording raw

APCDataDir = '/home/data/stud/periyasa/apc_data/';

% prepared data

APCOutDataDir = '/home/data/stud/periyasa/matlab_ws/';

end
