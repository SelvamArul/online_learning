--[[
 Author: Arul Selvam Periyasamy
 Mail: arulselvam@uni-bonn.de
 This file implements  Multi-GPI traning of the data

 To enable multi-GPU training, the following changes are done
 1. The foward passes of the network0 and networkd are concatenated and stored in the cache/disk *** currently supports
 only cache
 2. The network (the trainable component) is cloned to multiple GPU
 3. The DataParallelTable is used implement parallel training

--]]


require 'torch'		-- duh
require 'nn'		-- neural networks
require 'nngraph'
require 'optim'		-- optimization
require 'image'		-- image processing, load, save, scale, ...
require 'cunn'		-- cuda. required for GPU train/test
require 'lfs'		-- file system: create dir, etc.
require 'util.misc'	-- Anton's helper functions
require 'apc'		-- For test code
require 'io'        -- For log file writing
require 'cutorch'
require 'sys'
-- optnet = require 'optnet' -- optimize net

matio = require 'matio'  -- Matlab IO interface

-- nngraph.setDebug(true)  -- uncomment for debug mode
torch.setdefaulttensortype('torch.FloatTensor')

PD = 1

-- disable cross GPU checks
-- This is needed to enable MultiGPU training
print ('================================================>')
cutorch.setKernelPeerToPeerAccess(true)
print ('Disabled cross GPU checks')
print ('================================================>')

function create_network(nb_output, upsample)
    local filter_size = 11 -- fixed filter size
    local pad = (filter_size-1) / 2
    local net = nn.Sequential();  -- make a multi-layer structure
    net:add(nn.SpatialConvolutionMM(256+96,128,filter_size,filter_size,1,1,pad)) -- (256+96) -> 96
    net:add(nn.PReLU())
    net:add(nn.Dropout(params.dropout))
    net:add(nn.SpatialConvolutionMM(128,64,filter_size,filter_size,1,1,pad)) -- 96 -> 32
    net:add(nn.PReLU())
    net:add(nn.SpatialConvolutionMM(64,nb_output,filter_size,filter_size,1,1,pad)) -- 32x120x160 -> Clx120x160
    net:add(nn.SpatialUpSamplingNearest(upsample))
    net:add(nn.Transpose({2,3},{3,4})) -- HxWxCl
    net:add(nn.View(-1, nb_output)) -- (H*W)xCl
    net:add(nn.LogSoftMax())
    net:add(nn.View(-1, params.imHeight*params.imWidth, nb_output)) -- (H*W)xCl

    if params.cuda_device > 0 then net:cuda() end
    return net
end --function create_network

function convertTable2Tensor(table)
  local tensorSize = table[1]:size()
  local tensorSizeTable = {-1}
  for i=1,tensorSize:size(1) do
    tensorSizeTable[i+1] = tensorSize[i]
  end
  merge=nn.Sequential()
    :add(nn.JoinTable(1))
    :add(nn.View(unpack(tensorSizeTable)))

  return merge:forward(table)
end


--[[
-- This function loads the pre-trained networks and does a forward pass of all the images in the dataset
-- This step is not parallelised
-- The results of the forward pass is concatenated and stored to the cache/disk
-- The variables follows the following convention: all variables that live in CPU start with h_ (meaning host)
-- and all variables that live in GPU start with d_ (meaning device)
--]]
function cache_pretranined_forward()
    local nfiles = train_data.imArray:size(1)
    local h_cached_table = {}
    local inputs = train_data.imArray
    local features = hha_train.hha

    cutorch.setDevice(1)
    network0 = network0:cuda()
    networkd = networkd:cuda()
    print ('===============================>')
    print ('Forward pass on training data')
    for ii=1,nfiles do
        xlua.progress(ii, nfiles)	-- progress bar
        local color_image = inputs[{ii,{1,3},{},{}}]
        local depth_image2 = features[{ii,{},{},{}}]
        local input = network0:forward(color_image)	-- process RGB
        local inputd2 = networkd:forward(depth_image2):squeeze()   -- HHA
        local input_concat = torch.cat(input,inputd2,1):squeeze()  -- concat RGB,  HHA
        h_cached_table[#h_cached_table+1] = input_concat
    end
    -- check what type(host/device) DataParallelTable expects
    -- and move to h_cached GPU if needed
    -- seems DataParallelTable needs variables on device
    cutorch.setDevice(1) -- *** decide on the GPU# ***

    --convert table to tensor
    h_cached_tensor = convertTable2Tensor(h_cached_table)

    print ('===============================>')
    print ('Forward pass on test data')

    nfiles = test_data.imArray:size(1)
    local h_test_cached_table = {}
    inputs = test_data.imArray
    features = hha_test.hha
    for ii=1,nfiles do
        xlua.progress(ii, nfiles)	-- progress bar
        local color_image = inputs[{ii,{1,3},{},{}}]
        local depth_image2 = features[{ii,{},{},{}}]
        local input = network0:forward(color_image)	-- process RGB
        local inputd2 = networkd:forward(depth_image2):squeeze()   -- HHA
        local input_concat = torch.cat(input,inputd2,1):squeeze()  -- concat RGB,  HHA
        h_test_cached_table[#h_test_cached_table+1] = input_concat
    end

    h_test_cached_tensor = convertTable2Tensor(h_test_cached_table)

	network0 = {}
	networkkd = {}
	collectgarbage('collect')
end


--[[
-- This function accepts a model, and # GPU and creates a DPT by copying the model to # GPUs
-- This function also modifies the model argument
--]]
function create_DataParallelNetwork(model)
  print('converting module to nn.DataParallelTable')
  local model_single = model
  model = nn.DataParallelTable(1)
  for k,v in ipairs(GPU_LIST) do
     	print ('Device ',v)
	cutorch.setDevice(v)
     model:add(model_single:clone():cuda(), v)
  end
    cutorch.setDevice(params.cuda_device_train) -- *** decide on the default GPU ***
  return model
end

-- Train a Neural Network
function train_network()


    local bs = #GPU_LIST * 2-- batchsize

    -- hyper-parameters
    local lR = params.learning_rate / torch.sqrt(bs)
    local optimConfig = {learningRate = params.learning_rate,
    momentum = params.momentum,
    learningRateDecay = params.lr_decay}

    -- count number of train instances
    local nfiles = train_data.imArray:size(1)

    local weights = torch.Tensor(#classes):fill(1)

    -- set loss weights anti-proportional to frequency
    -- TODO: Try median frequency ratio!
    for c=1,#classes-1 do  weights[c] = 1-train_ratios[c] end
    if params.setting == 'shelf' then weights[#classes] = 0 end -- unknown class
    -- (only ignore for shelf because objects outside are not annotated. For box, there are no objects outside box)


    local critNLL =  nn.ClassNLLCriterion(weights)
    local criterion = critNLL
--`    criterion.sizeAverage = false

    if params.cuda_device >0 then criterion = criterion:cuda() end

    -- assign input and target
    local inputs = train_data.imArray
    local features = hha_train.hha
    local targets = train_data.labelArray

    print ( getModelSignature())

    local train_loss_logger = optim.Logger('logs/train_loss_' .. getModelSignature())
    train_loss_logger:setNames{'Loss'}
    train_loss_logger:style{'+-' }

    local train_gradNorm_logger = optim.Logger('logs/train_gradNorm_' .. getModelSignature())
    train_gradNorm_logger:setNames{'GradNorm'}
    train_gradNorm_logger:style{'+-' }

    validation_f1_logger = optim.Logger('logs/validation_f1_'.. getModelSignature())
    validation_f1_logger:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    validation_f1_logger:style{'+-', '+-', '+-' }

    train_f1_logger = optim.Logger('logs/train_f1_'.. getModelSignature())
    train_f1_logger:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    train_f1_logger:style{'+-', '+-', '+-' }

    validation_f1_logger_all = optim.Logger('logs/validation_f1_all'.. getModelSignature())
    validation_f1_logger_all:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    validation_f1_logger_all:style{'+-', '+-', '+-' }

    train_f1_logger_all = optim.Logger('logs/train_f1_all'.. getModelSignature())
    train_f1_logger_all:setNames{'Avg_precesion', 'Avg_recall', 'Avg_F1'}
    train_f1_logger_all:style{'+-', '+-', '+-'}

    -- call the forward pass
    cache_pretranined_forward()

    -- create a DPT
    network = create_DataParallelNetwork(network)

    local parameters,gradParameters = network:getParameters()
    network:training()	-- set flag for dropout
    print( "Training the network with "..nfiles.." files and "..parameters:nElement().. " parameters" )


    --reshape the targets
    targets_reshaped = torch.CudaTensor(nfiles,params.imWidth * params.imHeight)
	
	cutorch.setDevice(1)

    for ii=1,nfiles do
	   targets_reshaped[ii] = targets[ii]:clone():reshape(params.imWidth * params.imHeight)
    end
	print('targets_reshaped ',targets_reshaped:getDevice())
--    targets_reshaped = targets_reshaped:clone():cuda()

    for ep=1,params.epochs do
        EPOCH = ep
        local shuffle = torch.randperm(nfiles)
        for ii = 1,nfiles,bs do
            xlua.progress(ii, nfiles)
            local t = shuffle[ii]

--            local color_image = inputs[{t,{1,3},{},{}}]
--            local depth_image2 = features[{t,{},{},{}}]
--            cutorch.setDevice(params.cuda_device_train)
--            local input = network0:forward(color_image)	-- process RGB
--            local inputd2 = networkd:forward(depth_image2):squeeze()   -- HHA
--            local input_concat = torch.cat(input,inputd2,1):squeeze()  -- concat RGB,  HHA
--            cutorch.setDevice(params.cuda_device)
--            collectgarbage('collect')
--            input_concat = input_concat:clone():cuda()

            local t_end = math.min(t+bs-1,targets:size()[1])

            --local target_image = targets[{{t, t_end},{},{}}]
	   --  cutorch.setDevice(3)
            local target = targets_reshaped[{{t, t_end},{}}]
		
--            target = target:reshape(bs * params.imWidth * params.imHeight)

            local input_concat = h_cached_tensor[{{t, t_end}}]:clone():cuda()

		print ('copied input_concat')
		os.execute("sleep " .. tonumber(PD))
--            print ('******************************************************************************************')
--          print ('target ')
--            print (target:getDevice())
--            print (target:size())
--            print ('input_concat')
--            print (input_concat:getDevice())
--            print (input_concat:size())
--            print ('newtork')i
		network:training()
            local nweights, ngrads = network:parameters()

--            print (type(nweights))
--            print ('********************Cuda**********************************************************************')
            -- create closure to evaluate f(X) and df/dX
            local loss = 0
		print('Callig cutorch synchronize')
		os.execute("sleep " .. tonumber(PD))

            cutorch.synchronize()

            local feval = function(x)
                -- get new parameters
                if x ~= parameters then parameters:copy(x) end
                collectgarbage()

                -- reset gradients
                --gradParameters:zero()
		print (' network zerogradparams')
                os.execute("sleep " .. tonumber(PD))
		network:zeroGradParameters()
	
                

                -- f is the average of all criterions
                -- evaluate function for complete mini batch
		print('forward ')
		os.execute("sleep " .. tonumber(PD))
                local output = network:forward(input_concat)    -- run forward pass
                local df_do  = torch.CudaTensor(output:size()):fill(0)
                local err  = 0
--                print ('output_size ',output:size())
--                print ('input_concat SIZE ',input_concat:size())
		print('calling criterion')
		os.execute("sleep " .. tonumber(PD))
                for im=1,input_concat:size(1) do
			print ('criterion forward ')
			os.execute("sleep " .. tonumber(PD))
		            local err_ = criterion:forward(output[{im}], target[{im}])	-- compute loss
                	print('criterion backward')
			os.execute("sleep " .. tonumber(PD))
				err = err + err_
                	df_do[im] =  criterion:backward(output[im], target[im])
		        end
                loss = loss + err
		loss = loss / bs
                -- estimate df/dW
		print('calling network: backward')
		os.execute("sleep " .. tonumber(PD))

                network:backward(input_concat, df_do)		-- update parameters

                --display result
                --local win_input = showImage(color_image, win_input)
                --local win_target = showImage(colorizeLabels(target_image:float()), win_target)

               -- local _,predicted_labels = torch.max(output,2)
               -- predicted_labels = torch.reshape(predicted_labels:squeeze():float(),params.imHeight,params.imWidth)
               -- predicted_labels = colorizeLabels(predicted_labels:float()):float()
                --local win_pl = showImage(predicted_labels, win_pl)

                local woohooString = ''
                pm(string.format('Training loss: %.4f, %s', loss, woohooString), 2)

		gradParameters:div(input_concat:size(1))
                --clip the gradients to avoid exploding gradients problem
                gradParameters:clamp(-params.grad_clip,params.grad_clip)

                if loss < 0.5 then train_loss_logger:add{loss} else train_loss_logger:add{0.5} end
                train_loss_logger:plot()
                train_gradNorm_logger:add{torch.norm(gradParameters)}
                train_gradNorm_logger:plot()

                return err,gradParameters
            end -- feval lambda function

            _,current_loss = optim.sgd(feval, parameters, optimConfig)

            cutorch.synchronize()
            collectgarbage('collect')
        end -- ii

        print(string.format('Loss: %.4f  Epoch: %d   grad-norm: %.4f',
        current_loss[1], ep, torch.norm(parameters)/torch.norm(gradParameters)))


        if (ep % 10) == 0 or ep == params.epochs then
		print ('test / validation ' , ep)
		os.execute("sleep " .. tonumber(PD))
            local filename = getModelFilename()
            if ep < params.epochs then filename = getModelFilename(ep) end

            pm('Saving checkpoint to '..filename .. '...')
            network:clearState()
            --torch.save(filename, network)

            --pm('Training')
            --test_predictor_all(classes, 'train')
            pm('Validation...')
            test_predictor_all(classes, 'validation')
        end -- if epoch output
        collectgarbage('collect')

    end --epochs
    print('Training completed')
end -- function train_network

-- mode is switch between training data and validation data
-- mode possible values  =  train / validation
function test_predictor_all(classes, mode)
    print('Predict all images mode ======>' .. mode)
    if mode ~= 'validation' and mode ~= 'train' then
        print('Fatal!!! mode is not validation or train')
        abort()
    end

    testConfusion  = optim.ConfusionMatrix(classes)
    testConfusion2 = torch.zeros(#classes,#classes)

    local bs = #GPU_LIST * 2-- batchsize

    --switch the network to evaluate mode(turns off DropOut)
    network:evaluate()

    local nfiles, inputs, features, targets
    if mode == 'train' then
        nfiles = train_data.imArray:size(1)
        targets = train_data.labelArray
	print ( targets:size())
    else -- mode validate
        nfiles = test_data.imArray:size(1)
        targets = test_data.labelArray
    end

    for t = 1,nfiles,bs do
        xlua.progress(t, nfiles)	-- progress bar

        local t_end = math.min(t+bs-1,targets:size()[1])
        local target = targets[{{t, t_end}, {}, {}}]
	
	local input_concat
        if mode=='train' then
            input_concat = h_cached_tensor[{{t, t_end}}]:clone():cuda()
        else
            input_concat = h_test_cached_tensor[{{t, t_end}}]:clone():cuda()
        end

        local output = network:forward(input_concat)	-- run forward pass

        for im=1,input_concat:size(1) do
            local _,predicted_labels = torch.max(output[im],2)
            predicted_labels = torch.reshape(predicted_labels:squeeze():float(),params.imHeight,params.imWidth)

            --update_confusion2(testConfusion2, predicted_labels, target[im], classes)
            --image.save('predictions/'..EPOCH..'_'..t..'prediction.png', colorizeLabels(predicted_labels))
            --image.save('predictions/'..EPOCH..'_'..t..'target.png',colorizeLabels(target[im]))
        end
    end

    --torch.save('logs/confusion_matrix_'.. EPOCH .. '.dat',testConfusion2)

    testConfusion.mat = testConfusion2
    if #classes < 15 then print(testConfusion) end
    local precesion, recall, f1Score =  compute_F1score(testConfusion2)

    --torch.save('logs/'.. mode ..'_precesion_' .. EPOCH .. '.dat', precesion)
    --torch.save('logs/'.. mode ..'_recall_' .. EPOCH .. '.dat', recall)
    --torch.save('logs/'.. mode ..'_f1_' .. EPOCH .. '.dat', f1Score)
    --image.save('predictions/'.. mode ..'_confusion_matrix_'.. EPOCH ..'.png', testConfusion:render()) -- render matrix

    if mode == 'train' then
        train_f1_logger:add{ (precesion[CLASSBOOLINDICES]:sum()/#classes) * 100,
            (recall[CLASSBOOLINDICES]:sum()/#classes) * 100, (f1Score[CLASSBOOLINDICES]:sum()/#classes) * 100 }
        train_f1_logger:plot()
        train_f1_logger_all:add{ (precesion:sum()/#classes) * 100,
            (recall:sum()/#classes) * 100, (f1Score:sum()/#classes) * 100 }
        train_f1_logger_all:plot()

    else -- mode validation
        validation_f1_logger:add{ (precesion[CLASSBOOLINDICES]:sum()/#classes) * 100,
            (recall[CLASSBOOLINDICES]:sum()/#classes) * 100, (f1Score[CLASSBOOLINDICES]:sum()/#classes) * 100 }
        validation_f1_logger:plot()
        validation_f1_logger_all:add{ (precesion:sum()/#classes) * 100,
            (recall:sum()/#classes) * 100, (f1Score:sum()/#classes) * 100 }
        validation_f1_logger_all:plot()
    end
end

-- Input: confusion matrix
-- output: array of Precesion, Recall, F1 score for each object
function compute_F1score(confusionMatrix)
    local precesion, recall, f1Score
    precesion = torch.Tensor(#classes):zero()
    recall    = torch.Tensor(#classes):zero()
    f1Score   = torch.Tensor(#classes):zero()
    for i = 1, #classes do
        if confusionMatrix[i]:sum() ~= 0 then
            precesion[i]  =  confusionMatrix[{i,i}] / confusionMatrix[i]:sum() else precesion[i] = 0 end
        if confusionMatrix[{{},{i}}]:sum() ~= 0 then
            recall[i] = confusionMatrix[{i,i}] / confusionMatrix[{{},{i}}]:sum() else recall[i] = 0 end
        if precesion[i] + recall[i] ~= 0 then
            f1Score[i] = (2 * precesion[i] * recall[i]) / (precesion[i] + recall[i]) else f1Score[i] = 0 end
    end
    return precesion, recall, f1Score
end


function update_confusion2(testConfusion2, predicted_labels, target, classes)
    -- loop over all locations
    local confusion = torch.zeros(#classes,#classes)
    if params.cuda_device > 0 then
        predicted_labels = predicted_labels:float()
        target = target:float()
    end
    for i = 1,#classes-1 do
        for j = 1,#classes-1 do
            -- no. of predicted labels for class j
            local np = torch.sum(torch.eq(target,i):cmul(torch.eq(predicted_labels,j)))
            confusion[{i,j}] = np
        end
    end
    testConfusion2:add(confusion)
end -- function update_confusion2



function main()
    if runFromCmdLine() then setParams() end
    createAuxDirs() -- create required directories quietly
    print('Running segmentation of '..params.num_classes..' classes on '..params.setting)
    classes, classes_names = getClassInfo()
    getBooleanClassIndex()
    local dtype, use_cudnn = setup_gpus(params.cuda_device, 0)

    if params.mode=='train' then
        print('here true')
        train_data, test_data, hha_train, hha_test = dofile('apc_dataset.lua')
    else
        test_data, hha_test, cropInfo = processInput(params.imDir)
        --image.save('predictions/rgb.png', cropInfo.rgbIm)
        if params.cuda_device > 0 then
            test_data.imArray = test_data.imArray:cuda()
            test_data.labelArray = test_data.labelArray:cuda()
            hha_test.hha = hha_test.hha:cuda()
        end
    end

    cutorch.setDevice(1)
    loadPretrainedModels() -- load OverFeat model parameters
    cutorch.setDevice(1)


    if params.mode == 'train' then  -- train mode. Create network and off we go...
        network = create_network(#classes, params.downsample)
        -- graph.dot(network.fg, 'ForwardNet', 'MM_Network')
        train_network()
    else	-- testing

    local networkFileName = getModelFilename()
    pm('Loading '..networkFileName)
    network = torch.load(networkFileName)	-- load the network
    if params.cuda_device == 0 then
        network = network:float()
        print('converted to cpu')
    end

    collectgarbage()
    printGPUStatus('after garbage collection')

    test_predictor_all(network, test_data, hha_test, classes, params.downsample, params.jitter)
end

-- keep display and terminate
if runFromCmdLine() then sleep(1); os.exit()  end
end

-- if run on command line do main()
main()
