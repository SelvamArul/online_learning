import os
from glob import glob

PATH = '/home/arul/arul/apc/src/apc_data/shelf'
class_names = [
    'barkely_hide_bones',
    'box',
    'cherokee_easy_tee_shirt',
    'clorox_utility_brush',
    'cloud_b_plush_bear',
    'command_hooks',
    'cool_shot_glue_sticks',
    'crayola_24_ct',
    'creativity_chenille_stems',
    'dasani_water_bottle',
    'dove_beauty_bar',
    'dr_browns_bottle_brush',
    'easter_turtle_sippy_cup',
    'elmers_washable_no_run_school_glue',
    'expo_dry_erase_board_eraser',
    'fiskars_scissors_red',
    'fitness_gear_3lb_dumbbell',
    'folgers_classic_roast_coffee',
    'hanes_tube_socks',
    'i_am_a_bunny_book',
    'jane_eyre_dvd',
    'kleenex_paper_towels',
    'kleenex_tissue_box',
    'kyjen_squeakin_eggs_plush_puppies',
    'laugh_out_loud_joke_book',
    'oral_b_toothbrush_green',
    'oral_b_toothbrush_red',
    'peva_shower_curtain_liner',
    'platinum_pets_dog_bowl',
    'rawlings_baseball',
    'rolodex_jumbo_pencil_cup',
    'safety_first_outlet_plugs',
    'scotch_bubble_mailer',
    'scotch_duct_tape',
    'soft_white_lightbulb',
    'staples_index_cards',
    'ticonderoga_12_pencils',
    'up_glucose_bottle',
    'womens_knit_gloves',
    'woods_extension_cord' ]

def main():
    for i in class_names:
        result = [y for x in os.walk(PATH) for y in
              glob(os.path.join(x[0],'mask_' + i + '.png'))]
        print ('{:<40}'.format(i) + str(len(result)))

if __name__ =='__main__':
    main()


'''
barkely_hide_bones                      17
box                                     188
cherokee_easy_tee_shirt                 15
clorox_utility_brush                    17
cloud_b_plush_bear                      24
command_hooks                           17
cool_shot_glue_sticks                   16
crayola_24_ct                           18
creativity_chenille_stems               28
dasani_water_bottle                     18
dove_beauty_bar                         18
dr_browns_bottle_brush                  18
easter_turtle_sippy_cup                 18
elmers_washable_no_run_school_glue      22
expo_dry_erase_board_eraser             29
fiskars_scissors_red                    18
fitness_gear_3lb_dumbbell               18
folgers_classic_roast_coffee            18
hanes_tube_socks                        10
i_am_a_bunny_book                       17
jane_eyre_dvd                           16
kleenex_paper_towels                    9
kleenex_tissue_box                      18
kyjen_squeakin_eggs_plush_puppies       16
laugh_out_loud_joke_book                21
oral_b_toothbrush_green                 18
oral_b_toothbrush_red                   20
peva_shower_curtain_liner               18
platinum_pets_dog_bowl                  17
rawlings_baseball                       17
rolodex_jumbo_pencil_cup                16
safety_first_outlet_plugs               17
scotch_bubble_mailer                    18
scotch_duct_tape                        20
soft_white_lightbulb                    21
staples_index_cards                     27
ticonderoga_12_pencils                  18
up_glucose_bottle                       17
womens_knit_gloves                      18
woods_extension_cord                    17
'''