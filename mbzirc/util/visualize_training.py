import pyqtgraph as pg

import pyqtgraph.exporters
import numpy as np
import csv
import sys

app = pg.QtGui.QApplication([])

# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
graphWindows = {}

class Graph():
    pass

# read the arguments list of the form
# data_dir graph_name1 graph_datafile_name1 graph_name2 graph_datafile_name2 .. .. .. .. x_range


print (len(sys.argv))

for i in range(2, len(sys.argv) - 1, 2):

    g_name = sys.argv[i]

    g = Graph()
    g.file_name = sys.argv[1] + '/'+sys.argv[i+1]
    g.win = pg.GraphicsWindow(title=g_name)
    g.win.resize(1920 / 2 - 20, 1080 / 2 - 40)
    g.win.setWindowTitle(g_name)

    g.p = g.win.addPlot(title="Training")
    g.p.addLegend(offset=(-30, -30))
    g.p.enableAutoRange('xy', False)
    g.p.setXRange(0.0, float(sys.argv[len(sys.argv) - 1]), padding=0)
    g.p.setYRange(0.0, 100.0, padding=0)
    g.p.setLabels(bottom='Iterations')
    g.p.showGrid(x=True, y=True)

    g.curvePrecesion = g.p.plot(pen=(255, 0, 0), name='Precesion')
    g.curveRecall = g.p.plot(pen=(0, 255, 0), name='Recall')
    g.curveF1 = g.p.plot(pen=(0, 0, 255), name='F1 Score')

    g.firstUpdate = True
    graphWindows[g_name] = g




def update():
    global firstUpdate
    global graphWindows

    for k,g in graphWindows.iteritems():
        precision = []
        recall = []
        f1 = []
        with open(g.file_name) as tsv:
            reader = csv.reader(tsv, delimiter='\t')
            next(reader, None)  # skip the headers
            for line in reader:
                precision.append(float(line[0]))
                recall.append(float(line[1]))
                f1.append(float(line[2]))
        g.curvePrecesion.setData(precision)
        g.curveRecall.setData(recall)
        g.curveF1.setData(f1)

    if not g.firstUpdate:
        g.p.enableAutoRange('xy', False)
    g.firstUpdate = False



## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    timer = pg.QtCore.QTimer()
    timer.setInterval(100)
    timer.setSingleShot(True)
    timer.timeout.connect(update)

    update()
    timer.start()

    if sys.flags.interactive != 1 or not hasattr(pg.QtCore, 'PYQT_VERSION'):
        pg.QtGui.QApplication.exec_()
