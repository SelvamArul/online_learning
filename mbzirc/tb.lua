--[[
 Author: Arul Selvam Periyasamy
 Mail: arulselvam@uni-bonn.de
--]]


getmetatable('').__index = function(str,i) return string.sub(str,i,i) end
getmetatable('').__call = string.sub

cmd = torch.CmdLine()
    cmd:text('Training')
    cmd:text()
    cmd:option('-cuda_device',"1 2 3")
 params = cmd:parse(arg or {})

print (params.cuda_device)
print (type(params.cuda_device))
print (string.len(params.cuda_device))
for i=1,string.len(params.cuda_device) do
    if params.cuda_device[i] ~= ' ' then
        print (params.cuda_device[i])
    else
        print 'ommit'
    end
end
