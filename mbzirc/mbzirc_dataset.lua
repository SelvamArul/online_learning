require 'torch'
require 'math'
require 'image'
torch.setdefaulttensortype('torch.DoubleTensor')
matio = require 'matio'

DATASET = '/home/lperiyasa/mbzirc/mario_data/'
IMGW = 300
IMGH = 480

params.imHeight = IMGH
params.imWidth = IMGW

function runFromCmdLine()
  if arg~=nil then return true end
  return false
end

-- see if the file exists
local function file_exists(file)
	local f = io.open(file, "rb")
	if f then f:close() end
	return f ~= nil
end

-- get all lines from a file, returns an empty
-- list/table if the file does not exist
local function lines_from(file)
	if not file_exists(file) then return {} end
	local lines = {}
	for line in io.lines(file) do
		lines[#lines + 1] = line
	end
	return lines
end

function convertTable2Tensor(table)
	local tensorSize = table[1]:size()
	local tensorSizeTable = {-1}
	for i=1,tensorSize:size(1) do
		tensorSizeTable[i+1] = tensorSize[i]
	end
	merge=nn.Sequential()
	:add(nn.JoinTable(1))
	:add(nn.View(unpack(tensorSizeTable)))

	return merge:forward(table)
end

-- loads all the images and its ground truth annotations and returns table of table of two tensors
local function load_images(file)
	local table_img = {}
	local table_gt = {}
	for k,v in pairs(file) do
		local gray = image.scale(image.load(DATASET .. v ..'/gray.jpg'), IMGW, IMGH)
		local gt = image.scale(image.load(DATASET .. v ..'/mask_wrench_mouth.png'), IMGW, IMGH)
		gt[gt:eq(1)] = 2
		gt[gt:ne(2)] = 1
		table_img[#table_img + 1] = gray
		table_gt[#table_gt + 1] = gt
	end
	local table_all = {}
	table_all.imgs = convertTable2Tensor(table_img)
	table_all.gts = convertTable2Tensor(table_gt)
	return table_all

end

function load_data()

	local train_data = {}
	local test_data = {}

	local train_file = DATASET .. 'splits/w_mouth_train.txt'
	local test_file = DATASET .. 'splits/w_mouth_test.txt'
	local train_list = lines_from(train_file)
	local test_list = lines_from(test_file)

	train_data = load_images(train_list)
	test_data = load_images(test_list)

	return train_data, test_data
end


return load_data()


--if runFromCmdLine() then
--	load_data()
--end
